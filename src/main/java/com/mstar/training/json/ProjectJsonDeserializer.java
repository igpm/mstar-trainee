package com.mstar.training.json;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonComponent;
import org.springframework.boot.jackson.JsonObjectDeserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.mstar.training.beans.Project;
import com.mstar.training.repository.ProjectRepository;

@JsonComponent
public class ProjectJsonDeserializer extends JsonObjectDeserializer<Project> {

	@Autowired
	private ProjectRepository repository;
	
	@Override
	protected Project deserializeObject(JsonParser jsonParser, DeserializationContext context, ObjectCodec codec, JsonNode tree) throws IOException {

		String name = tree.asText();
		
		List<Project> matches = repository.findByName(name);
		
		if( matches.isEmpty() )
			throw new IOException( "No such project. Name? " + name );
		
		return matches.get( 0 );
	}

}
