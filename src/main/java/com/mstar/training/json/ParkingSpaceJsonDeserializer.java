package com.mstar.training.json;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonComponent;
import org.springframework.boot.jackson.JsonObjectDeserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.mstar.training.beans.ParkingSpace;
import com.mstar.training.repository.ParkingSpaceRepository;

@JsonComponent
public class ParkingSpaceJsonDeserializer extends JsonObjectDeserializer<ParkingSpace> {

	@Autowired
	private ParkingSpaceRepository repository;
	
	@Override
	protected ParkingSpace deserializeObject(JsonParser jsonParser, DeserializationContext context, ObjectCodec codec, JsonNode tree) throws IOException {
		
		JsonNode lot = tree.get("lot");
		JsonNode location = tree.get("location");
		
		List<ParkingSpace> matches = repository.findByLotAndLocation( lot.asInt(), location.asText() );
		
		if( matches.isEmpty() )
			throw new IOException( "No such parking space. Lot? " + lot.asInt() + " Location? " + location.asText() );
		
		return matches.get( 0 );
	}

}
