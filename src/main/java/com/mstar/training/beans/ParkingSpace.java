package com.mstar.training.beans;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ParkingSpace {

	@Id
	private Long id;
	
	private Integer lot;
	
	private String location;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getLot() {
		return lot;
	}

	public void setLot(Integer lot) {
		this.lot = lot;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
}
