package com.mstar.training.beans;

public enum EmployeeType {
	
	FULL_TIME, 
	PART_TIME, 
	CONTRACTUAL
	
}
