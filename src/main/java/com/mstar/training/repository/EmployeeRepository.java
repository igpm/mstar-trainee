package com.mstar.training.repository;

import org.springframework.data.repository.CrudRepository;

import com.mstar.training.beans.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

}
