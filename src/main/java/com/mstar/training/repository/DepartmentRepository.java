package com.mstar.training.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mstar.training.beans.Department;

public interface DepartmentRepository extends CrudRepository<Department, Long> {

	public List<Department> findByName(String name);
	
}
