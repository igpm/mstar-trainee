package com.mstar.training.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mstar.training.beans.Project;

public interface ProjectRepository extends CrudRepository<Project, Long> {

	public List<Project> findByName(String name);
	
}
