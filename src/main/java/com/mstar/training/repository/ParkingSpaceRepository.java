package com.mstar.training.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mstar.training.beans.ParkingSpace;

public interface ParkingSpaceRepository extends CrudRepository<ParkingSpace, Long> {

	public List<ParkingSpace> findByLotAndLocation(Integer lot, String location);
}
